<div class="col-md-12">
    <div class="box pad box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pilih feature</label>
                        <select class="form-control" style="width: 100%;" id="unrated_feature">
                            <?php foreach ($bunch_of_unrated_feature as $data_rating) { ?>
                                <option value="<?php echo $data_rating->features_id?>"><?php echo $data_rating->name?> - <?php echo $data_rating->features?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pilih feature yang akan di bandingkan</label>
                        <select class="form-control" style="width: 100%;" id="rated_feature">
                            <?php foreach ( $bunch_of_rated_feature as $feature) { ?>
                                <option value="<?php echo $feature->id?>"><?php echo $feature->features?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a href="#" data-toggle="tooltip" data-placement="top" title="Proses">
                    <button type="button" class="btn btn-flat btn-primary center-block" data-toggle="modal"
                            data-target="#modal-deviasi" onclick="countDeviation()">
                        <span class="fa fa-calculator" area-hidden="true"></span> Hitung
                    </button>
                </a>
            </div>
        </div>
    </div>
    <div class="box pad box-success">
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover dataTable data-posts">
                <thead>
                <tr>
                    <th width="1%"></th>
                    <th width="6%;">Kota</th>
                    <th width="3%;">Feature 1</th>
                    <th width="3%;">Feature 2</th>
                    <th width="3%;">Nilai Deviasi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach ( $bunch_of_nilai_deviasi as $nilai_deviasi) { ?>
                    <tr>
                        <td align="center;">
                            <?php
                            echo $no;
                            $no++;
                            ?>
                        </td>
                        <td align="center;"><?php echo $nilai_deviasi->name?></td>
                        <td align="center;"><?php echo $nilai_deviasi->feature_1?></td>
                        <td align="center;"><?php echo $nilai_deviasi->feature_2?></td>
                        <td align="center;"><?php echo $nilai_deviasi->deviation_result?></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-deviasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="text-align: center; font-weight: bold;">Niali Deviasi</h3>
                </div>
                <div class="modal-body">
                    <p class="lead" style="text-align: center;" id="nilai-deviasi">test</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" onclick="location.reload();">Kembali</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        /*

         */
        function countDeviation()
        {
            var unrated_feature  = $('#unrated_feature').val();
            var rated_feature    = $('#rated_feature').val();
            $.ajax({
                url         : "<?php echo base_url('Kota/countDeviation')?>/"+unrated_feature+"/"+rated_feature,
                type        : "GET",
                success     : function (data) {
                    var mydata          = $.parseJSON(data);
                    var nilai_deviasi   = mydata.nilai_deviasi.toFixed(2);
                    var cities_id  = mydata.cities_id;
                    var unrated_feature_id  = mydata.unrated_feature_id;
                    var rated_feature_id    = mydata.rated_feature_id;

                    var html    = "<p class='lead' style='text-align: center;'>"+nilai_deviasi+"</p>";
                    document.getElementById("nilai-deviasi").innerHTML = html;

                    $.ajax({
                        url:    "<?php echo base_url('Kota/storeDeviationResult')?>/"+cities_id+"/"+unrated_feature_id+"/"+rated_feature_id+"/"+nilai_deviasi,
                        type:   "POST"
                    });
                }
            });
        }
    </script>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

</div>