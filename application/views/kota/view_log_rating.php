<div class="col-md-12">
    <div class="box pad box-success">
        <div class="box-body">
                <table class="table table-bordered table-striped table-hover dataTable data-posts">
                    <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="6%;">Kota</th>
                        <th width="3%;">Feature</th>
                        <th width="3%;">Nilai Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ( $bunch_of_rating_city as $data_rating) { ?>
                        <tr>
                            <td align="center;">
                                <?php
                                echo $no;
                                $no++;
                                ?>
                            </td>
                            <td align="center;"><?php echo $data_rating->name?></td>
                            <td align="center;"><?php echo $data_rating->features?></td>
                            <td align="center;"><?php echo $data_rating->nilai_rating?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>        
    </div>
        
    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>        
</div>