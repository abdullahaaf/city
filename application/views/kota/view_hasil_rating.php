<div class="col-md-12">
    <div class="row" style="padding-right: 16px;">
        <div class="" style="margin-right: 0px; padding: 30px">
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control" id="feature_id">
                        <option value="">-- pilih feature --</option>
                        <?php foreach ($bunch_of_feature as $value) { ?>
                            <option value="<?php echo $value->id?>"><?php echo $value->features?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <select class="form-control" id="nilai_rating">
                        <option value="">-- pilih nilai rating --</option>
                        <?php foreach ($nilai_rating as $value) { ?>
                            <option value="<?php echo $value->nilai_rating?>"><?php echo $value->nilai_rating?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <select class="form-control" id="tipe_kota">
                        <option value="">-- pilih tipe kota --</option>
                        <?php foreach ($bunch_of_tipe_kota as $tipe_kota) { ?>
                            <option value="<?php echo $tipe_kota->id?>"><?php echo $tipe_kota->tipe_kota?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-success btn-flat" onclick="filter();">Filter</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div class="box pad box-success">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover dataTable data-posts">
                    <thead>
                    <tr>
                        <th width="3%"></th>
                        <th width="3%;">Nama Kota</th>
                        <th width="3%;">Tipe Kota</th>
                        <th width="3%;">Feature</th>
                        <th width="3%;">Nilai Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ( $bunch_of_rating as $data_rating) { ?>
                        <tr>
                            <td align="center;">
                                <?php
                                echo $no;
                                $no++;
                                ?>
                            </td>
                            <td align="center;"><?php echo $data_rating->name?></td>
                            <td align="center;"><?php echo $data_rating->tipe_kota?></td>
                            <td align="center;"><?php echo $data_rating->features?></td>
                            <td align="center;"><?php echo $data_rating->nilai_rating?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>
    <script type="text/javascript">
        function filter()
        {
            var features_id = $('#feature_id').val();
            var nilai_rating = $('#nilai_rating').val();
            var tipe_kota = $('#tipe_kota').val();
            window.location.href = "<?php echo base_url('Kota/indexHasilRating')?>/"+features_id+"/"+nilai_rating+"/"+tipe_kota;
        }
    </script>

</div>
