<div class="col-md-12">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><strong>Statistik Rating Kota Sedang</strong></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="kota_sedang" style="height:300px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                    <h3 class="box-title"><strong>  Statistik Rating Kota Besar</strong></h3>
                <div class="box-header with-border">

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="kota_besar" style="height:300px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                    <h3 class="box-title"><strong>  Statistik Rating Kota Metropolis</strong></h3>
                <div class="box-header with-border">

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="kota_metropolis" style="height:300px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- kota sedang -->
    <?php foreach ($infrastructure_sedang as $i) {
        $kota_sedang[] = $i->name;
        $rating_infrastructure_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($energy_sedang as $i) {
        $rating_energy_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($people_sedang as $i) {
        $rating_people_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($health_sedang as $i) {
        $rating_health_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($mobility_sedang as $i) {
        $rating_mobility_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($government_sedang as $i) {
        $rating_government_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($education_sedang as $i) {
        $rating_education_sedang[] = $i->nilai_rating;
    }?>

    <?php foreach ($technology_sedang as $i) {
        $rating_technology_sedang[] = $i->nilai_rating;
    }?>
    <!-- end kota sedang -->
    <!-- kota besar -->
    <?php foreach ($infrastructure_besar as $i) {
        $kota_besar[] = $i->name;
        $rating_infrastructure_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($energy_besar as $i) {
        $rating_energy_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($people_besar as $i) {
        $rating_people_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($health_besar as $i) {
        $rating_health_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($mobility_besar as $i) {
        $rating_mobility_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($government_besar as $i) {
        $rating_government_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($education_besar as $i) {
        $rating_education_besar[] = $i->nilai_rating;
    }?>

    <?php foreach ($technology_besar as $i) {
        $rating_technology_besar[] = $i->nilai_rating;
    }?>
    <!-- end kota besar -->
    <!-- kota metropolis -->
    <?php foreach ($infrastructure_metropolis as $i) {
        $kota_metropolis[] = $i->name;
        $rating_infrastructure_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($energy_metropolis as $i) {
        $rating_energy_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($people_metropolis as $i) {
        $rating_people_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($health_metropolis as $i) {
        $rating_health_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($mobility_metropolis as $i) {
        $rating_mobility_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($government_metropolis as $i) {
        $rating_government_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($education_metropolis as $i) {
        $rating_education_metropolis[] = $i->nilai_rating;
    }?>

    <?php foreach ($technology_metropolis as $i) {
        $rating_technology_metropolis[] = $i->nilai_rating;
    }?>
    <!-- end kota metropolis -->
    <script>

        var options = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
            responsive: true,
        };
        var kota_sedang = document.getElementById("kota_sedang").getContext('2d');
        var kota_besar = document.getElementById("kota_besar").getContext('2d');
        var kota_metropolis = document.getElementById("kota_metropolis").getContext('2d');
        var chart_sedang = new Chart(kota_sedang, {
            type: 'line',
            data: {
                labels: <?php echo json_encode($kota_sedang)?>,
                datasets: [
                    {
                        label: 'Smart Infrastructure',
                        data: <?php echo json_encode($rating_infrastructure_sedang)?>,
                        backgroundColor: 'red',
                        borderColor: 'red',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Energy',
                        data: <?php echo json_encode($rating_energy_sedang)?>,
                        backgroundColor: '#cddc39',
                        borderColor: '#cddc39',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart People',
                        data: <?php echo json_encode($rating_people_sedang)?>,
                        backgroundColor: '#00b0ff',
                        borderColor: '#00b0ff',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Health',
                        data: <?php echo json_encode($rating_health_sedang)?>,
                        backgroundColor: 'yellow',
                        borderColor: 'yellow',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Mobility',
                        data: <?php echo json_encode($rating_mobility_sedang)?>,
                        backgroundColor: '#ff6333',
                        borderColor: '#ff6333',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Government',
                        data: <?php echo json_encode($rating_government_sedang)?>,
                        backgroundColor: 'blue',
                        borderColor: 'blue',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Education',
                        data: <?php echo json_encode($rating_education_sedang)?>,
                        backgroundColor: 'green',
                        borderColor: 'green',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Technology',
                        data: <?php echo json_encode($rating_technology_sedang)?>,
                        backgroundColor: '#ffc400',
                        borderColor: '#ffc400',
                        borderWidth: 3,
                        fill: false,
                    }
                ]
            },
            options: options,
        });
        var chart_besar = new Chart(kota_besar, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($kota_besar)?>,
                datasets: [
                    {
                        label: 'Smart Infrastructure',
                        data: <?php echo json_encode($rating_infrastructure_besar)?>,
                        backgroundColor: 'red',
                        borderColor: 'red',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Energy',
                        data: <?php echo json_encode($rating_energy_besar)?>,
                        backgroundColor: '#cddc39',
                        borderColor: '#cddc39',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart People',
                        data: <?php echo json_encode($rating_people_besar)?>,
                        backgroundColor: '#00b0ff',
                        borderColor: '#00b0ff',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Health',
                        data: <?php echo json_encode($rating_health_besar)?>,
                        backgroundColor: 'yellow',
                        borderColor: 'yellow',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Mobility',
                        data: <?php echo json_encode($rating_mobility_besar)?>,
                        backgroundColor: '#ff6333',
                        borderColor: '#ff6333',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Government',
                        data: <?php echo json_encode($rating_government_besar)?>,
                        backgroundColor: 'blue',
                        borderColor: 'blue',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Education',
                        data: <?php echo json_encode($rating_education_besar)?>,
                        backgroundColor: 'green',
                        borderColor: 'green',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Technology',
                        data: <?php echo json_encode($rating_technology_besar)?>,
                        backgroundColor: '#ffc400',
                        borderColor: '#ffc400',
                        borderWidth: 3,
                        fill: false,
                    }
                ]
            },
            options: options,
        });
        var chart_metropolis = new Chart(kota_metropolis, {
            type: 'bar',
            data: {
                labels: <?php echo json_encode($kota_metropolis)?>,
                datasets: [
                    {
                        label: 'Smart Infrastructure',
                        data: <?php echo json_encode($rating_infrastructure_metropolis)?>,
                        backgroundColor: 'red',
                        borderColor: 'red',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Energy',
                        data: <?php echo json_encode($rating_energy_metropolis)?>,
                        backgroundColor: '#cddc39',
                        borderColor: '#cddc39',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart People',
                        data: <?php echo json_encode($rating_people_metropolis)?>,
                        backgroundColor: '#00b0ff',
                        borderColor: '#00b0ff',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Health',
                        data: <?php echo json_encode($rating_health_metropolis)?>,
                        backgroundColor: 'yellow',
                        borderColor: 'yellow',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Mobility',
                        data: <?php echo json_encode($rating_mobility_metropolis)?>,
                        backgroundColor: '#ff6333',
                        borderColor: '#ff6333',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Government',
                        data: <?php echo json_encode($rating_government_metropolis)?>,
                        backgroundColor: 'blue',
                        borderColor: 'blue',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Education',
                        data: <?php echo json_encode($rating_education_metropolis)?>,
                        backgroundColor: 'green',
                        borderColor: 'green',
                        borderWidth: 3,
                        fill: false,
                    },
                    {
                        label: 'Smart Technology',
                        data: <?php echo json_encode($rating_technology_metropolis)?>,
                        backgroundColor: '#ffc400',
                        borderColor: '#ffc400',
                        borderWidth: 3,
                        fill: false,
                    }
                ]
            },
            options: options,
        });
    </script>

</div>