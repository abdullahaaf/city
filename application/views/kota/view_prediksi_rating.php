<div class="col-md-12">
    <div class="box pad box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="form-control" style="width: 100%;" id="unrated_feature_id">
                            <?php foreach ($bunch_of_unrated_feature as $data_rating) { ?>
                                <option value="<?php echo $data_rating->cities_id?>"><?php echo $data_rating->name?> - <?php echo $data_rating->features?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Proses">
                            <button type="button" class="btn btn-flat btn-primary pull-left" data-toggle="modal"
                                    data-target="#modal-prediksi" onclick="countPrediction();">
                                <span class="fa fa-calculator" area-hidden="true"></span> Hitung Prediksi
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box pad box-success">
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover dataTable data-posts">
                <thead>
                <tr>
                    <th width="1%"></th>
                    <th width="6%;">Kota</th>
                    <th width="3%;">Feature</th>
                    <th width="3%;">Nilai Prediksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($bunch_of_hasil_prediksi as $prediksi) { ?>
                        <tr>
                            <td align="center;">
                                <?php
                                echo $no;
                                $no++;
                                ?>
                            </td>
                            <td align="center;"><?php echo $prediksi->name?></td>
                            <td align="center;"><?php echo $prediksi->features?></td>
                            <td align="center;"><?php echo $prediksi->nilai_rating?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="modal-prediksi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="margin-top:10%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="text-align: center; font-weight: bold;">Prediksi Nilai Rating</h3>
                </div>
                <div class="modal-body">
                    <p class="lead" style="text-align: center;" id="nilai-rating">test</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal" onclick="location.reload();">Kembali</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

    <script type="text/javascript">
        function countPrediction()
        {
            var unrated_feature_id  = $("#unrated_feature_id").val();
            $.ajax({
                url     : "<?php echo base_url('Kota/countPrediction')?>/"+unrated_feature_id,
                type    : "GET",
                success : function (data)
                {
                    var mydata  = $.parseJSON(data);
                    var cities_id  = mydata.cities_id;
                    var features_id       = mydata.features_id;
                    var nilai_rating    = mydata.nilai_rating;

                    var html    = "<p class='lead' style='text-align: center;'>"+nilai_rating+"</p>";
                    document.getElementById("nilai-rating").innerHTML = html;

                    $.ajax({
                        url: "<?php echo base_url('Kota/storePredictionResult')?>/"+cities_id+"/"+features_id+"/"+nilai_rating,
                        type: "POST"
                    });
                }
            });
        }
    </script>
</div>