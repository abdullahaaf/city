<div class="col-md-12">
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                    <div class="small-box bg-lime">
                        <div class="inner">
                            <h3>2,83</h3>
                             <p>Kuadrat Rating</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tags"></i>
                        </div>
                        <a href="#" class="small-box-footer"></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-6">
                    <div class="small-box bg-fuchsia-active">
                        <div class="inner">
                            <h3>42</h3>
                             <p>Jumlah Rating</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tags"></i>
                        </div>
                        <a href="#" class="small-box-footer"></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-6">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>0,08</h3>
                             <p>Hasil RMSE</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tags"></i>
                        </div>
                        <a href="#" class="small-box-footer"></a>
                    </div>
                </div>
            </div>
</div>