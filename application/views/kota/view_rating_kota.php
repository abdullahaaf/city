<div class="col-md-12">
    <!--    modal tambah data rating-->
    <div class="modal fade" id="modal-tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="<?php echo base_url('Kota/storeRating')?>" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title" id="exampleModalLabel">Tambah Data Rating Baru</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table-striped table-hover table">
                            <div class="form-group">
                                <label>Nama Kota</label>
                                <select class="form-control" style="width: 100%;" name="kota">
                                    <?php foreach ( $bunch_of_kota as $kota) { ?>
                                        <option value="<?php echo $kota->id?>"><?php echo $kota->name?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Feature</label>
                                <select class="form-control" style="width: 100%;" name="feature">
                                    <?php foreach ( $bunch_of_feature as $feature) { ?>
                                        <option value="<?php echo $feature->id?>"><?php echo $feature->features?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nilai Feature</label>
                                <input type="text" name="nilai_feature" class="form-control" style="width: 100%;">
                            </div>
                        </table>
                    </div>
                    <div class="modal-footer" style="margin-right: 15px;">
                        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Kembali
                        </button>
                        <input type="submit" id ="simpan" class="btn btn-flat btn-success" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--    end modal tambah data rating-->
    <div class="row" style="padding-right: 16px;">
        <div class="pull-right" style="margin-right: 0px;">
            <button type="button" class="btn btn-flat btn-warning fixed-button" data-toggle="modal" data-target="#modal-tambah" id="fixedbutton">
                <span class="glyphicon glyphicon-plus" area-hidden="true"></span>
                <span class="hidden-xs">Tambah Data</span>
            </button>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <div class="box pad box-success">
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover dataTable data-posts">
                    <thead>
                    <tr>
                        <th width="1%"></th>
                        <th width="6%;">Nama Kota</th>
                        <th width="3%;">Feature</th>
                        <th width="3%;">Nilai Rating</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ( $bunch_of_rating as $data_rating) { ?>
                        <tr>
                            <td align="center;">
                                <?php
                                echo $no;
                                $no++;
                                ?>
                            </td>
                            <td align="center;"><?php echo $data_rating->name?></td>
                            <td align="center;"><?php echo $data_rating->features?></td>
                            <td align="center;"><?php echo $data_rating->nilai_rating?></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" class="init" language="javascript">
        $(function () {
            $('.data-posts').DataTable();
        });
    </script>

</div>
