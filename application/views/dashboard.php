<div class="col-md-12">
    <div class="row">
        <div class="col-md-6">
            <div class="box pad box-success">
                <div class="box-header">
                    <div class="pull-left" style="margin-right: 0px;">
                        <p class="lead">Daftar Kota</p>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dataTable" id="data-posts">
                        <thead>
                        <tr>
                            <th width="2%;"></th>
                            <th width="6%;">Kota</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ( $bunch_of_kota as $kota) { ?>
                            <tr>
                                <td align="center;">
                                    <?php
                                    echo $no;
                                    $no++;
                                    ?>
                                </td>
                                <td align="center;"><?php echo $kota->name?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box pad box-success">
                <div class="box-header">
                    <div class="pull-left" style="margin-right: 0px;">
                        <p class="lead">Daftar Feature</p>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dataTable" id="data-posts">
                        <thead>
                        <tr>
                            <th width="2%;"></th>
                            <th width="5%;">Nama Feature</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no = 1;
                        foreach ( $bunch_of_feature as $feature) { ?>
                            <tr>
                                <td align="center;">
                                    <?php
                                    echo $no;
                                    $no++;
                                    ?>
                                </td>
                                <td align="center;"><?php echo $feature->features?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <script type="text/javascript" class="init" language="javascript">
            $(function () {
                $('#data-posts').DataTable();
            });
        </script>

    </div>
</div>
