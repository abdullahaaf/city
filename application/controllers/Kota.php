<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['bunch_of_kota']                 = $this->Mkota->getAllKota();
        $this->data['bunch_of_feature']              = $this->Mfeature->getAllFeature();
        $this->data['bunch_of_rating']               = $this->Mkota->getRatingKota();
        $this->data['sites']                         = "Data Rating kota";
        $this->data['pages']                         = "Data Rating kota";
        $this->data['content']                       = $this->load->view('kota/view_rating_kota',$this->data,true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexHasilRating($features_id, $nilai_rating, $id_tipe_kota)
    {
        $this->data['nilai_rating']                  = $this->Mkota->getRatingValue();
        $this->data['bunch_of_feature']              = $this->Mfeature->getAllFeature();
        $this->data['bunch_of_tipe_kota']            = $this->Mkota->getTipeKota();
        $this->data['bunch_of_rating']               = $this->Mkota->getFilteredRating($features_id,$nilai_rating, $id_tipe_kota);
        $this->data['sites']                         = "Data Rating kota";
        $this->data['pages']                         = "Data Rating kota";
        $this->data['content']                       = $this->load->view('kota/view_hasil_rating',$this->data,true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexDeviasi()
    {
        $this->data['bunch_of_rated_feature'] = $this->Mkota->getRatedFeatureByCityTarget();
        $this->data['bunch_of_unrated_feature']  = $this->Mkota->getUnratedFeature();
        $this->data['bunch_of_nilai_deviasi']   = $this->Mkota->getDeviation();
        $this->data['sites']                    = "Proses Perhitungan Nilai Deviasi";
        $this->data['pages']                    = "Proses perhitungan Nilai Deviasi";
        $this->data['content']                  = $this->load->view('kota/view_deviasi', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexPrediksi()
    {
        $this->data['bunch_of_hasil_prediksi']  = $this->Mkota->getPredictionResult();
        $this->data['bunch_of_unrated_feature']  = $this->Mkota->getUnratedFeature();
        $this->data['sites']                    = "Proses Perhitungan Prediksi Nilai Rating";
        $this->data['pages']                    = "Proses perhitungan Prediksi Nilai Rating";
        $this->data['content']                  = $this->load->view('kota/view_prediksi_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexLogRating()
    {
        $this->data['sites']                    = "Daftar Kota yang telah di prediksi nilai rating nya";
        $this->data['pages']                    = "Log Rating";
        $this->data['bunch_of_rating_city']   = $this->Mkota->getPredictionResult();
        $this->data['content']                  = $this->load->view('kota/view_log_rating', $this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function indexChartRatingPerFeature()
    {
        // kota sedang
        $this->data['infrastructure_sedang'] = $this->Mkota->getStatsOfFeatureInfrastructureSedang();
        $this->data['energy_sedang'] = $this->Mkota->getStatsOfFeatureEnergySedang();
        $this->data['people_sedang'] = $this->Mkota->getStatsOfFeaturePeopleSedang();
        $this->data['health_sedang'] = $this->Mkota->getStatsOfFeatureHealthSedang();
        $this->data['mobility_sedang'] = $this->Mkota->getStatsOfFeatureMobilitySedang();
        $this->data['government_sedang'] = $this->Mkota->getStatsOfFeatureGovernmentSedang();
        $this->data['education_sedang'] = $this->Mkota->getStatsOfFeatureEducationSedang();
        $this->data['technology_sedang'] = $this->Mkota->getStatsOfFeatureTechnologySedang();
        // end kota sedang
        // kota besar
        $this->data['infrastructure_besar'] = $this->Mkota->getStatsOfFeatureInfrastructureBesar();
        $this->data['energy_besar'] = $this->Mkota->getStatsOfFeatureEnergyBesar();
        $this->data['people_besar'] = $this->Mkota->getStatsOfFeaturePeopleBesar();
        $this->data['health_besar'] = $this->Mkota->getStatsOfFeatureHealthBesar();
        $this->data['mobility_besar'] = $this->Mkota->getStatsOfFeatureMobilityBesar();
        $this->data['government_besar'] = $this->Mkota->getStatsOfFeatureGovernmentBesar();
        $this->data['education_besar'] = $this->Mkota->getStatsOfFeatureEducationBesar();
        $this->data['technology_besar'] = $this->Mkota->getStatsOfFeatureTechnologyBesar();
        // end kota besar
        // kota metropolis
        $this->data['infrastructure_metropolis'] = $this->Mkota->getStatsOfFeatureInfrastructureMetropolis();
        $this->data['energy_metropolis'] = $this->Mkota->getStatsOfFeatureEnergyMetropolis();
        $this->data['people_metropolis'] = $this->Mkota->getStatsOfFeaturePeopleMetropolis();
        $this->data['health_metropolis'] = $this->Mkota->getStatsOfFeatureHealthMetropolis();
        $this->data['mobility_metropolis'] = $this->Mkota->getStatsOfFeatureMobilityMetropolis();
        $this->data['government_metropolis'] = $this->Mkota->getStatsOfFeatureGovernmentMetropolis();
        $this->data['education_metropolis'] = $this->Mkota->getStatsOfFeatureEducationMetropolis();
        $this->data['technology_metropolis'] = $this->Mkota->getStatsOfFeatureTechnologyMetropolis();
        // end kota metropolis
        $this->data['sites']    = "Statistik rating kota per feature";
        $this->data['pages']    = "Statistik Data";
        $this->data['content']  = $this->load->view('kota/view_chart_rating_per_feature',$this->data, true);
        $this->load->view('layouts/layout', $this->data);
    }

    public function storeRating()
    {
        $kota           = $this->input->post('kota');
        $feature             = $this->input->post('feature');
        $nilai_feature  = $this->input->post('nilai_feature');

        $cek_rating = $this->Mkota->checkExistRating($kota,$feature);
        if ($cek_rating > 0) {
            redirect('Kota');
        } else {
            if ($nilai_feature > 0){
                if ($nilai_feature > 0 and $nilai_feature <= 0.16){
                    $nilai_rating = 1;
                }elseif ($nilai_feature > 0.16 and $nilai_feature <=0.84){
                    $nilai_rating = 2;
                }elseif ($nilai_feature > 0.84 and $nilai_feature <= 11.84){
                    $nilai_rating = 3;
                }elseif ($nilai_feature > 11.84){
                    $nilai_rating = 4;
                }
                $this->Mkota->storeRating($kota,$feature,$nilai_rating);
            }else{
                $this->Mkota->storeUnratedFeature($kota,$feature,$nilai_feature);
            }
            redirect(base_url('Kota'));
        }
    }

    public function storeDeviationResult($cities_id,$unrated_feature_id,$rated_feature_id,$deviation_result)
    {
        $check_deviasi  = $this->Mkota->checkExistDeviation($cities_id,$unrated_feature_id,$rated_feature_id);
        if ($check_deviasi > 0) {
            redirect(base_url('Kota/indexDeviasi'));
        }else {
            $this->Mkota->storeDeviationResult($cities_id,$unrated_feature_id,$rated_feature_id,$deviation_result);
        }
    }

    public function storePredictionResult($cities_id,$features_id,$nilai_rating)
    {
        $check_rating   = $this->Mkota->checkExistPredictedRating($cities_id,$features_id);
        if ($check_rating > 0) {
            redirect(base_url('Kota/indexPrediksi'));
        }else{
            $this->Mkota->storePredictionResult($cities_id,$features_id,$nilai_rating);
            $this->Mkota->storeRating($cities_id,$features_id,$nilai_rating);
            $this->Mkota->deleteUnratedCity($cities_id);
        }
    }

    /**
     * barisan kode dibawah ini merupakan implementasi
     * algoritma slope one
     */

    public function countDeviation($unrated_feature_id,$rated_feature_id)
    {
        $a = 0;
        $rating_from_target_feature  = $this->Mkota->getRatingFromTargetFeature($unrated_feature_id,$rated_feature_id);
        $rating_from_compared_feature    = $this->Mkota->getRatingFromComparedFeature($unrated_feature_id,$rated_feature_id);
        $city_target    = $this->Mkota->getUnratedCity($unrated_feature_id);
        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang akan
         * diprediksi menjadi satu array
         */
        foreach ($rating_from_target_feature as $value) {
            $rating_value_target_feature[] = $value->nilai_rating;
        }

        /**
         * kode dibawah ini untuk menjadikan
         * nilai rating dari produk yang
         * akan dibandingkan menjadi
         * satu array
         */
        foreach ($rating_from_compared_feature as $value) {
            $rating_value_compared_feature[] = $value->nilai_rating;
        }

         echo json_encode($rating_value_target_feature);
         echo "<br>";
         echo json_encode($rating_value_compared_feature);

        /**
         * kode dibawah ini digunakan untuk menjadikan
         * hasil pengurangan nilai rating produk yang
         * akan diprediksi nilai rating nya dengan produk
         * yang akan dibandingkan menjadi satu array
         */
        foreach ($rating_value_target_feature as $key => $value) {
            $substract_result[]   = $value - $rating_value_compared_feature[$key];
        }

        /**
         * kode dibawah ini digunakan untuk
         * menjumlahkan hasil dari pengurangan
         * diatas
         */
        foreach ($substract_result as  $value) {
            $a  = $a+=$value;
        }

        $number_of_cities          = count($rating_value_compared_feature);
        $deviation_result               = $a/$number_of_cities;

        $array_result   = array(
            'unrated_feature_id' => $unrated_feature_id,
            'rated_feature_id'   => $rated_feature_id,
            'nilai_deviasi'     => $deviation_result,
            'cities_id'           => $city_target->cities_id
        );
        echo json_encode($array_result);
    }

    public function countPrediction($cities_id)
    {
        $nilai_atas = 0;
        $nilai_bawah = 0;

        $unrated_feature         = $this->Mkota->getUnratedFeatureByCity($cities_id);
        foreach ($unrated_feature as $feature){
            $unrated_feature_id  = $feature->features_id;
        }

        /*
         * kode ini digunakan untuk menjadikan rating dari distributor
         * target menjadi single array
         */
        $city_rating    = $this->Mkota->getCityRating($cities_id);
        foreach ($city_rating as $rating) {
            $rating_value_of_city_target[] = $rating->nilai_rating;
        }

        /*
         * kode ini digunakan untuk menjadikan nilai deviasi produk
         * yang akan diprediksi nilai rating nya dengan produk yang
         * telah dirating oleh distributor target menjadi single array
         */
        $user_deviation = $this->Mkota->getCityDeviation($cities_id);
        foreach ($user_deviation as $deviasi) {
            $deviation_result[]           = $deviasi->deviation_result;
            $rated_feature_in_deviation[]  = $deviasi->rated_feature_id;
        }

        foreach($rated_feature_in_deviation as $key => $value) {
            $number_of_city_rated_both_feature[] =   $this->Mkota->countRatedFeature($unrated_feature_id,$value);
        }
        $number_of_feature_rated_by_city_target   = $this->Mkota->countRatedFeatureByCityTarget($cities_id);
        for($i = 0 ; $i < $number_of_feature_rated_by_city_target ; $i++) {
            $prediksi[] = ($deviation_result[$i] + $rating_value_of_city_target[$i]) * $number_of_city_rated_both_feature[$i];
        }

        foreach ($prediksi as $value) {
            $nilai_atas = $nilai_atas += $value;
        }

        foreach ($number_of_city_rated_both_feature as $value) {
            $nilai_bawah = $nilai_bawah += $value;
        }

        $hasil_prediksi     = $nilai_atas/$nilai_bawah;

        /*
         * kode ini digunakan untuk membuat array dari
         * data hasil prediksi nilai rating produk
         */
        $prediction_result  = array(
            'cities_id'    => $cities_id,
            'features_id'         => $unrated_feature_id,
            'nilai_rating'      => number_format($hasil_prediksi,2)
        );
        echo json_encode($prediction_result);

    }
}
