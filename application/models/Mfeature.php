<?php
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 27/07/18
 * Time: 17:54
 */

class Mfeature extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllFeature()
    {
        $this->db->from('features');
        $query = $this->db->get();
        return $query->result();
    }
}