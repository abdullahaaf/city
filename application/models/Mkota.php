<?php
/**
 * Created by PhpStorm.
 * User: abdullahaaf
 * Date: 27/07/18
 * Time: 17:53
 */

class Mkota extends CI_Model
{
    /**
    * fungsi ini digunakan untuk mendapatkan
    * data tipe kota
    */
    public function getTipeKota()
    {
        $this->db->order_by('tipe_kota.id','ASC');
        $query = $this->db->get('tipe_kota');
        return $query->result();
    }
    /*
     * fungsi ini digunakan untuk menampilkan
     * semua kota
     */
    public function getAllKota()
    {
        $this->db->from('cities');
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan
     * nilai rating
     */
    public function getRatingValue()
    {
        $sql = "select distinct(nilai_rating) as nilai_rating from rating order by nilai_rating asc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * fungsi ini digunakan untuk
     * menampilkan feature yang telah
     * dirating oleh kota target
     */
    public function getRatedFeatureByCityTarget()
    {
        $sql = "SELECT features.id, features.features, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN features ON features.id = rating.features_id";
        $sql .= " WHERE rating.cities_id = (SELECT cities_id FROM not_rated_city)";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan
     * semua data rating kota
     */
    public function getRatingKota()
    {
        $this->db->join('cities','cities.id = rating.cities_id');
        $this->db->join('features','features.id = rating.features_id');
        $this->db->order_by('name','ASC');
        $this->db->from('rating');
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan
     * data rating yang telah di filter
     */
    public function getFilteredRating($features_id, $nilai_rating, $id_tipe_kota)
    {
        $this->db->where('features_id', $features_id);
        $this->db->where('nilai_rating', $nilai_rating);
        $this->db->where('cities.id_tipe_kota', $id_tipe_kota);
        $this->db->join('cities','cities.id = rating.cities_id');
        $this->db->join('features','features.id = rating.features_id');
        $this->db->join('tipe_kota','tipe_kota.id =  cities.id_tipe_kota');
        $this->db->order_by('name','ASC');
        $this->db->from('rating');
        $query = $this->db->get();
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan produk
     * yang belum mendapatkan nilai rating
     */
    public function getUnratedFeature()
    {
        $this->db->join('cities','cities.id = not_rated_city.cities_id');
        $this->db->join('features','features.id = not_rated_city.features_id');
        $this->db->order_by('cities.name','ASC');
        $this->db->select('not_rated_city.cities_id,cities.name,features.features, not_rated_city.features_id');
        $query = $this->db->get('not_rated_city');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan produk
     * yang belum dirating oleh user target
     */
    public function getUnratedFeatureByCity($cities_id)
    {
        $sql    = "SELECT features_id FROM not_rated_city WHERE cities_id =".$cities_id;
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan hanya
     * produk yang sudah di rating
     */
    public function getRatedProduk()
    {
        $sql = "SELECT * FROM produk WHERE produk.produk_id NOT IN";
        $sql .= "(SELECT not_rated_distributor.produk_id FROM not_rated_distributor)";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan area
     * yang belum memberi nilai rating pada produk
     */
    public function getUnratedCity($feature_id)
    {
        $this->db->where('features_id', $feature_id);
        $this->db->select('cities_id');
        $query  = $this->db->get('not_rated_city');
        return $query->row();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data nilai
     * deviasi setiap produk air minum
     */
    public function getDeviation()
    {
        $sql = "SELECT cities.name, p1.features as feature_1, p2.features AS feature_2, dev.deviation_result
FROM dev_rating AS dev
JOIN cities ON cities.id = dev.cities_id
JOIN features AS p1 ON p1.id = dev.unrated_feature_id
JOIN features AS p2 on p2.id = dev.rated_feature_id ORDER BY p2.id ASC ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menampilkan data
     * hasil prediksi
     */
    function getPredictionResult()
    {
        $this->db->join('cities','cities.id = prediction_result.cities_id');
        $this->db->join('features','features.id = prediction_result.features_id');
        $query  = $this->db->get('prediction_result');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menambahkan rating
     * baru untuk setiap kota
     */
    public function storeRating($kota_id,$feature_id, $nilai_rating)
    {
        $data = array(
            'cities_id'        => $kota_id,
            'features_id'             => $feature_id,
            'nilai_rating'          => $nilai_rating,
        );
        $this->db->insert('rating', $data);
    }

    /*
     * fungsi ini digunakan untuk menambahkan produk
     * yang belum mendapatkan nilai rating
     */
    public function storeUnratedFeature($cities_id, $features_id, $nilai_rating)
    {
        $data = array(
            'cities_id'        => $cities_id,
            'features_id'             => $features_id,
            'nilai_rating'           => $nilai_rating,
        );
        $this->db->insert('not_rated_city', $data);
    }

    /*
     * fungsi ini digunakan untuk menyimpan nilai deviasi
     * kedalam tabel deviasi
     */
    function storeDeviationResult($cities_id, $unrated_feature_id, $rated_feature_id, $deviation_result)
    {
        $data = array(
            'cities_id'    => $cities_id,
            'unrated_feature_id' => $unrated_feature_id,
            'rated_feature_id'   => $rated_feature_id,
            'deviation_result'            => $deviation_result
        );
        $this->db->insert('dev_rating', $data);
    }

    /*
     * fungsi ini digunakan untuk menyimpan hasil prediksi
     * nilai rating
     */
    function storePredictionResult($cities_id, $features_id, $nilai_rating)
    {
        $data   = array(
            'cities_id'    => $cities_id,
            'features_id'         => $features_id,
            'nilai_rating'      => $nilai_rating
        );
        $this->db->insert('prediction_result', $data);
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan diprediksi
     * nilai rating nya
     */
    public function getRatingFromTargetFeature($unrated_feature_id, $rated_feature_id)
    {
        $sql    =  "SELECT cities_id,nilai_rating FROM rating WHERE cities_id ";
        $sql    .= "IN (SELECT cities_id FROM rating WHERE features_id = {$rated_feature_id})"; 
        $sql    .= "AND features_id = $unrated_feature_id ORDER BY cities_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk
     * menadapatkan nilai rating
     * produk yang akan dibandingkan
     * dengan produk target
     */
    public function getRatingFromComparedFeature($unrated_feature_id,$rated_feature_id)
    {
        $sql    = "SELECT * FROM rating WHERE cities_id IN ";
        $sql    .= "(SELECT cities_id FROM rating WHERE features_id = {$unrated_feature_id}) ";
        $sql    .= "AND features_id = {$rated_feature_id} ORDER BY cities_id ASC";
        $query  = $this->db->query($sql);
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk menghitung distributor
     * yang merating kedua produk
     * produk yang akan di prediksi
     * dan produk yang dibandingkan
     */
    function countRatedFeature($unrated_feature_id, $rated_feature_id)
    {
        $hasil = 0;
        $sql    = "SELECT COUNT(*) as jumlah_kota FROM rating WHERE cities_id ";
        $sql    .= "IN (SELECT cities_id FROM rating WHERE ";
        $sql    .= "features_id = {$unrated_feature_id}) AND features_id = $rated_feature_id";
        $query = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil = $q->jumlah_kota;
        }
        return $hasil;
    }

    /**
     * fungsi ini digunakan untuk menampilkan
     * jumlah produk yang telah dirating
     * oleh distributor target
     */
    public function countRatedFeatureByCityTarget($cities_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as jumlah_feature from rating ";
        $sql    .= "where cities_id = {$cities_id}";
        $query  = $this->db->query($sql);
        foreach($query->result() as $q) {
            $hasil =  $q->jumlah_feature;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk mendapatkan nilai rating
     * produk-produk yang telah dirating oleh user target
     */
    function getCityRating($cities_id)
    {
        $this->db->where('cities_id',$cities_id);
        $this->db->order_by('cities_id','ASC');
        $query  = $this->db->get('rating');
        return $query->result();
    }
    
    /*
     * fungsi ini digunakan untuk mendapatkan nilai deviasi
     * dari produk yang akan diprediksi nilai rating nya
     * dengan produk-produk yang telah dirating
     * oleh user target
     */
    function getCityDeviation($cities_id)
    {
        $this->db->where('cities_id', $cities_id);
        $this->db->order_by('rated_feature_id', 'ASC');
        $query  = $this->db->get('dev_rating');
        return $query->result();
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data rating yang dimasukkan
     * sudah ada didalam table atau belum
     */
    public function checkExistRating($kota,$feature)
    {
        $hasil  = 0;
        $sql    = "SELECT COUNT(*) as rating from rating where ";
        $sql    .= "cities_id={$kota} and features_id={$feature}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data deviasi sudah dimasukkan atau belum
     */
    public function checkExistDeviation($cities_id,$unrated_feature_id,$rated_feature_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as deviasi from dev_rating where ";
        $sql    .= "cities_id={$cities_id} and unrated_feature_id={$unrated_feature_id} ";
        $sql    .= "and rated_feature_id={$rated_feature_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->deviasi;
        }
        return $hasil;
    }

    /*
     * fungsi ini digunakan untuk memeriksa
     * apakah data prediksi rating sudah dimasukkan atau belum
     */
    public function checkExistPredictedRating($cities_id, $features_id)
    {
        $hasil  = 0;
        $sql    = "select count(*) as rating from prediction_result where ";
        $sql    .= "cities_id={$cities_id} and features_id={$features_id}";
        $query  = $this->db->query($sql);
        foreach ($query->result() as $q) {
            $hasil  = $q->rating;
        }
        return $hasil;
    }

    /**
    * fungsi ini digunakan untuk menghapus
    * data yang sudah diprediksi nilai rating nya
    */
    public function deleteUnratedCity($cities_id)
    {
        $this->db->where('cities_id', $cities_id);
        $this->db->delete('not_rated_city');
    }

    /*
     * kode dibawah ini untuk keperluan pembuatan
     * grafik
      */
    // grafik untuk kota sedang
    public function getStatsOfFeatureEnergySedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 1 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureInfrastructureSedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 2 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeaturePeopleSedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 3 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureHealthSedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 4 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureMobilitySedang()
    {
       $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 5 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureGovernmentSedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 6 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureEducationSedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 7 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureTechnologySedang()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 8 AND cities.id_tipe_kota = 2";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    // end of grafik kota sedang
    // grafik untuk kota besar
    public function getStatsOfFeatureEnergyBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 1 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureInfrastructureBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 2 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeaturePeopleBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 3 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureHealthBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 4 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureMobilityBesar()
    {
       $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 5 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureGovernmentBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 6 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureEducationBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 7 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureTechnologyBesar()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 8 AND cities.id_tipe_kota = 3";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    // end of kota besar
    // grafik untuk kota metropolis
    public function getStatsOfFeatureEnergyMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 1 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureInfrastructureMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 2 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeaturePeopleMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 3 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureHealthMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 4 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureMobilityMetropolis()
    {
       $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 5 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureGovernmentMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 6 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureEducationMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 7 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getStatsOfFeatureTechnologyMetropolis()
    {
        $sql = "SELECT cities.name, rating.nilai_rating FROM rating";
        $sql .= " JOIN cities ON cities.id = rating.cities_id";
        $sql .= " JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota";
        $sql .= " WHERE rating.features_id = 8 AND cities.id_tipe_kota = 4";
        $sql .= " ORDER BY rating.cities_id ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    // end of kota metropolis
}