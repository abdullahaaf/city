-- hapus data yang tidak merating lebih dari 4 feature
DELETE FROM real_rating WHERE cities_id IN (7,8,11,12,15,16,17,18,19,20,34,35,36,39,40,43,45,50,52,53,57,58,61,62,63,64,65,68,70,74,75,78,79,80,81,82,85)

-- query untuk menampilkan feature yang dirating oleh kota target
SELECT features.id, features.features, rating.nilai_rating
FROM rating
JOIN cities ON cities.id = rating.cities_id
JOIN features ON features.id = rating.features_id
WHERE rating.cities_id = (SELECT cities_id FROM not_rated_city)

-- query tabel rating ditambah filter tipe kota
SELECT * FROM rating
JOIN cities ON cities.id = rating.cities_id
JOIN features ON features.id = rating.features_id
JOIN tipe_kota ON tipe_kota.id = cities.id_tipe_kota
WHERE
rating.features_id = 1 AND
rating.nilai_rating = 1 AND
cities.id_tipe_kota = 2