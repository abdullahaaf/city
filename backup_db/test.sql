-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2018 at 10:56 AM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.0.31-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `area` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`area_id`, `area`) VALUES
(1, 'SURABAYA'),
(2, 'SIDOARJO'),
(3, 'MALANG'),
(4, 'MOJOKERTO'),
(5, 'JOMBANG'),
(6, 'TRENGGALEK'),
(7, 'BANGKALAN'),
(8, 'JEMBER'),
(9, 'LUMAJANG'),
(10, 'PROBOLINGGO'),
(11, 'SITUBONDO');

-- --------------------------------------------------------

--
-- Table structure for table `area_prediction_result`
--

CREATE TABLE `area_prediction_result` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area_prediction_result`
--

INSERT INTO `area_prediction_result` (`id`, `area_id`, `produk_id`, `nilai_rating`) VALUES
(2, 1, 6, '3.48');

-- --------------------------------------------------------

--
-- Table structure for table `dev_rating_area`
--

CREATE TABLE `dev_rating_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `unrated_produk_id` int(11) NOT NULL,
  `rated_produk_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dev_rating_area`
--

INSERT INTO `dev_rating_area` (`id`, `area_id`, `unrated_produk_id`, `rated_produk_id`, `result`) VALUES
(1, 1, 6, 1, '0.00'),
(2, 1, 6, 2, '0.14'),
(3, 1, 6, 3, '-1.57'),
(5, 1, 6, 5, '-0.29');

-- --------------------------------------------------------

--
-- Table structure for table `dev_rating_distributor`
--

CREATE TABLE `dev_rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `unrated_produk_id` int(11) NOT NULL,
  `rated_produk_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dev_rating_distributor`
--

INSERT INTO `dev_rating_distributor` (`id`, `distributor_id`, `unrated_produk_id`, `rated_produk_id`, `result`) VALUES
(1, 3, 1, 2, '0.67'),
(2, 3, 1, 3, '-1.17'),
(3, 3, 1, 4, '0.33'),
(4, 3, 1, 5, '-0.67'),
(5, 3, 1, 6, '1.33');

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `distributor_id` int(11) NOT NULL,
  `nama_distributor` varchar(23) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `distributor`
--

INSERT INTO `distributor` (`distributor_id`, `nama_distributor`, `region`) VALUES
(1, ' BP NIDHOM SURABAYA ', 'sidoarjo'),
(2, ' BP MUHAMMAD ', 'sidoarjo'),
(3, ' BP ANDI TROPODO ', 'sidoarjo'),
(4, ' BP NOVI SURABAYA ', 'sidoarjo'),
(5, ' IBU SOFI ', 'sidoarjo'),
(6, ' BP FAUZAN SEPANJANG ', 'sidoarjo'),
(7, ' BP NANANG NURSYAH ', 'sidoarjo'),
(8, ' H MAKSUM ', 'sidoarjo'),
(9, ' IBU NONY ', 'sidoarjo'),
(10, ' DA\'I MART ', 'sidoarjo'),
(11, ' BP Totok ', 'sidoarjo'),
(12, ' MAS THOMY SIDOARJO ', 'sidoarjo'),
(13, ' BP SAIKHUL HUDA ', 'sidoarjo'),
(14, ' GUS ALI ', 'sidoarjo'),
(15, ' IBU LILIS / BP NODHOM ', 'sidoarjo'),
(16, ' IBU ARDIANA ', 'sidoarjo'),
(17, ' SABILILLAH MART ', 'sidoarjo'),
(18, ' BP AINUDDIN ', 'sidoarjo'),
(19, 'UST AFIF', 'BANGKALAN');

-- --------------------------------------------------------

--
-- Table structure for table `distributor_prediction_result`
--

CREATE TABLE `distributor_prediction_result` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributor_prediction_result`
--

INSERT INTO `distributor_prediction_result` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
(1, 3, 1, '3.498'),
(2, 1, 3, '3.17\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `not_rated_area`
--

CREATE TABLE `not_rated_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `ratingValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `not_rated_distributor`
--

CREATE TABLE `not_rated_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `ratingValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `not_rated_distributor`
--

INSERT INTO `not_rated_distributor` (`id`, `distributor_id`, `produk_id`, `ratingValue`) VALUES
(1, 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `produk_id` int(11) NOT NULL,
  `nama_produk` varchar(255) NOT NULL,
  `jenis_produk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`produk_id`, `nama_produk`, `jenis_produk`) VALUES
(1, '19', 'galon'),
(2, '120', 'gelas'),
(3, '240', 'gelas'),
(4, '330', 'botol'),
(5, '600', 'botol'),
(6, '1500', 'botol');

-- --------------------------------------------------------

--
-- Table structure for table `rating_area`
--

CREATE TABLE `rating_area` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_area`
--

INSERT INTO `rating_area` (`id`, `area_id`, `produk_id`, `nilai_rating`) VALUES
(4, 1, 1, 4),
(5, 1, 2, 3),
(6, 1, 3, 4),
(8, 1, 5, 4),
(9, 2, 1, 4),
(10, 2, 2, 4),
(11, 2, 3, 4),
(13, 2, 5, 4),
(14, 2, 6, 3),
(15, 4, 1, 1),
(16, 4, 2, 1),
(17, 4, 3, 3),
(19, 4, 5, 1),
(20, 4, 6, 2),
(21, 5, 1, 2),
(22, 5, 2, 1),
(23, 5, 3, 4),
(25, 5, 5, 2),
(26, 5, 6, 1),
(27, 6, 1, 1),
(28, 6, 2, 1),
(29, 6, 3, 4),
(31, 6, 5, 1),
(32, 6, 6, 1),
(33, 7, 1, 4),
(34, 7, 2, 2),
(35, 7, 3, 4),
(37, 7, 5, 4),
(38, 7, 6, 3),
(39, 8, 1, 3),
(40, 8, 2, 2),
(41, 8, 3, 4),
(43, 8, 5, 3),
(44, 8, 6, 3),
(45, 9, 1, 1),
(46, 9, 2, 4),
(47, 9, 3, 4),
(49, 9, 5, 3),
(50, 9, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rating_distributor`
--

CREATE TABLE `rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating_distributor`
--

INSERT INTO `rating_distributor` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
(1, 1, 1, '2'),
(2, 1, 2, '1'),
(3, 1, 4, '2'),
(4, 1, 5, '3'),
(5, 1, 6, '1'),
(13, 3, 2, '4'),
(14, 3, 3, '4'),
(15, 3, 4, '4'),
(16, 3, 5, '4'),
(17, 3, 6, '1'),
(18, 4, 1, '4'),
(19, 4, 2, '4'),
(20, 4, 3, '4'),
(21, 4, 4, '4'),
(22, 4, 5, '4'),
(23, 4, 6, '1'),
(24, 8, 1, '4'),
(25, 8, 2, '2'),
(26, 8, 3, '4'),
(27, 8, 4, '2'),
(28, 8, 5, '3'),
(29, 10, 1, '2'),
(30, 10, 2, '2'),
(31, 10, 3, '4'),
(32, 10, 4, '2'),
(33, 10, 5, '3'),
(34, 10, 6, '1'),
(35, 13, 1, '1'),
(36, 13, 2, '1'),
(37, 13, 3, '4'),
(38, 13, 4, '1'),
(39, 13, 5, '3'),
(40, 13, 6, '1'),
(41, 15, 1, '3'),
(42, 15, 2, '2'),
(43, 15, 3, '4'),
(44, 15, 4, '3'),
(45, 15, 5, '4'),
(46, 15, 6, '1'),
(47, 8, 6, '3'),
(48, 1, 3, '3.17'),
(50, 3, 1, '3.498');

-- --------------------------------------------------------

--
-- Table structure for table `real_rating_distributor`
--

CREATE TABLE `real_rating_distributor` (
  `id` int(11) NOT NULL,
  `distributor_id` int(11) NOT NULL,
  `produk_id` int(11) NOT NULL,
  `nilai_rating` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_rating_distributor`
--

  INSERT INTO `real_rating_distributor` (`id`, `distributor_id`, `produk_id`, `nilai_rating`) VALUES
  (1, 1, 1, '2'),
  (2, 1, 2, '1'),
  (3, 1, 4, '2'),
  (4, 1, 5, '3'),
  (5, 1, 6, '1'),
  (13, 3, 2, '4'),
  (14, 3, 3, '4'),
  (15, 3, 4, '4'),
  (16, 3, 5, '4'),
  (17, 3, 6, '1'),
  (18, 4, 1, '4'),
  (19, 4, 2, '4'),
  (20, 4, 3, '4'),
  (21, 4, 4, '4'),
  (22, 4, 5, '4'),
  (23, 4, 6, '1'),
  (24, 8, 1, '4'),
  (25, 8, 2, '2'),
  (26, 8, 3, '4'),
  (27, 8, 4, '2'),
  (28, 8, 5, '3'),
  (29, 10, 1, '2'),
  (30, 10, 2, '2'),
  (31, 10, 3, '4'),
  (32, 10, 4, '2'),
  (33, 10, 5, '3'),
  (34, 10, 6, '1'),
  (35, 13, 1, '1'),
  (36, 13, 2, '1'),
  (37, 13, 3, '4'),
  (38, 13, 4, '1'),
  (39, 13, 5, '3'),
  (40, 13, 6, '1'),
  (41, 15, 1, '3'),
  (42, 15, 2, '2'),
  (43, 15, 3, '4'),
  (44, 15, 4, '3'),
  (45, 15, 5, '4'),
  (46, 15, 6, '1'),
  (47, 8, 6, '3'),
  (48, 1, 3, '3'),
  (50, 3, 1, '3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `unrated_produk_id` (`unrated_produk_id`),
  ADD KEY `rated_produk_id` (`rated_produk_id`);

--
-- Indexes for table `dev_rating_distributor`
--
ALTER TABLE `dev_rating_distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`distributor_id`);

--
-- Indexes for table `distributor_prediction_result`
--
ALTER TABLE `distributor_prediction_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`),
  ADD KEY `produk_id` (`produk_id`);

--
-- Indexes for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`distributor_id`),
  ADD KEY `itemID` (`produk_id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `rating_area`
--
ALTER TABLE `rating_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wilayah_id` (`area_id`),
  ADD KEY `item_id` (`produk_id`);

--
-- Indexes for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`distributor_id`),
  ADD KEY `itemID` (`produk_id`);

--
-- Indexes for table `real_rating_distributor`
--
ALTER TABLE `real_rating_distributor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dev_rating_distributor`
--
ALTER TABLE `dev_rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `distributor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `distributor_prediction_result`
--
ALTER TABLE `distributor_prediction_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `rating_area`
--
ALTER TABLE `rating_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `real_rating_distributor`
--
ALTER TABLE `real_rating_distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `area_prediction_result`
--
ALTER TABLE `area_prediction_result`
  ADD CONSTRAINT `area_prediction_result_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `area_prediction_result_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dev_rating_area`
--
ALTER TABLE `dev_rating_area`
  ADD CONSTRAINT `dev_rating_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dev_rating_area_ibfk_2` FOREIGN KEY (`unrated_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dev_rating_area_ibfk_3` FOREIGN KEY (`rated_produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_rated_area`
--
ALTER TABLE `not_rated_area`
  ADD CONSTRAINT `not_rated_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `not_rated_area_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `not_rated_distributor`
--
ALTER TABLE `not_rated_distributor`
  ADD CONSTRAINT `not_rated_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `not_rated_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating_area`
--
ALTER TABLE `rating_area`
  ADD CONSTRAINT `rating_area_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_area_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rating_distributor`
--
ALTER TABLE `rating_distributor`
  ADD CONSTRAINT `rating_distributor_ibfk_1` FOREIGN KEY (`distributor_id`) REFERENCES `distributor` (`distributor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rating_distributor_ibfk_2` FOREIGN KEY (`produk_id`) REFERENCES `produk` (`produk_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
