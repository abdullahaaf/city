-- fungsi predict item
SELECT dev_rating_distributor.produk_id_1 as produk, SUM(dev_rating_distributor.sum + dev_rating_distributor.count*rating_distributor.ratingValue) / SUM(dev_rating_distributor.count) AS avgrat FROM rating_distributor, dev_rating_distributor WHERE rating_distributor.distributor_id=1 AND dev_rating_distributor.produk_id_1 NOT IN (SELECT rating_distributor.produk_id FROM rating_distributor WHERE rating_distributor.distributor_id=1) AND dev_rating_distributor.produk_id_2 = rating_distributor.produk_id GROUP BY dev_rating_distributor.produk_id_1 ORDER BY `avgrat` DESC LIMIT 20

-- query untuk rating produk botol di area pengiriman
SELECT area.area, produk.nama_produk, rating_area.ratingValue FROM rating_area
JOIN area ON area.area_id = rating_area.area_id
JOIN produk ON produk.produk_id = rating_area.produk_id
WHERE produk.jenis_produk = 'botol'

-- query menampilkan nilai deviasi
SELECT distributor.nama_distributor, p1.nama_produk as produk_1, p2.nama_produk AS produk_2, dev.result
FROM dev_rating_distributor AS dev
JOIN distributor ON distributor.distributor_id = dev.distributor_id
JOIN produk AS p1 ON p1.produk_id = dev.unrated_produk_id
JOIN produk AS p2 on p2.produk_id = dev.rated_produk_id

-- query untuk keperluan RMSE
SELECT distributor.nama_distributor, produk.nama_produk, nilai_rating FROM real_rating_distributor JOIN distributor ON distributor.distributor_id = real_rating_distributor.distributor_id JOIN produk ON produk.produk_id = real_rating_distributor.produk_id ORDER BY distributor.nama_distributor ASC

-- query menampilkan user yang merating kedua produk
SELECT * FROM rating_distributor WHERE distributor_id IN (SELECT distributor_id FROM rating_distributor WHERE produk_id = 3) AND produk_id = 1 ORDER BY distributor_id ASC